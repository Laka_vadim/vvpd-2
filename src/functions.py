def counting_vowels(line):
  vowel = 'аеёиоуыэюя'
  vowel_counter = {}

  for char in line:
    if char in vowel:
      vowel_counter[char] = vowel_counter.setdefault(char,0)+1

  sorted_result = sorted(vowel_counter.items(), reverse=True,key=lambda x : x[1])

  for key,val in sorted_result:
    print(key,val)

def counting_consonants_test(line):
  consonant = 'бвгджзклмнпрстфхцчъь'
  consonant_counter = {}

  for char in line:
    if char in consonant:
      consonant_counter[char] = consonant_counter.setdefault(char,0)+1

  return sorted(consonant_counter.items(), reverse=True,key=lambda x : x[1])

def counting_consonants(line):

  for key,val in counting_consonants_test(line):
    print(key,val)

def counting_letters(line):
  counting_consonants(line)
  counting_vowels(line)
