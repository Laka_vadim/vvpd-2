from functions import counting_consonants, counting_vowels, counting_letters


def menu():
    choice = -1
    while choice != 0:
        print(
            '\n'
            'Подсчет.\n'
            '1. Подсчет количества гласных.\n'
            '2. Подсчет количества согласных.\n'
            '3. Подсчет количества букв.\n'
            '0. Выход.'
        )
        choice = int(input('Введите номер меню: '))

        if choice == 1:
            line = input('Введите строку: ')
            counting_vowels(line)

        elif choice == 2:
            line = input('Введите строку: ')
            counting_consonants(line)

        elif choice == 3:
            line = input('Введите строку: ')
            counting_letters(line)

        elif choice == 0:
            print('\nценоК.')

        else:
            print('\nНе верная команда. Введите число от 0 до 6.')


if __name__ == '__main__':
    menu()
