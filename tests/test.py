import inspect
import os
import sys


currentdir = os.path.dirname(
    os.path.abspath(
        inspect.getfile(
            inspect.currentframe()
        )
    )
)
parrentdir = os.path.dirname(currentdir)
src_path = os.path.join(parrentdir, 'src')
sys.path.insert(0, src_path)


from functions import counting_consonants_test


def test_counting_consonants():
    """
    Тест функции counting_consonants().
    """
    assert counting_consonants_test("ба") == [('б', 1)]
